
import UIKit
import UserNotifications

//This extension is made in order to handle the Notifications:
extension MainTableViewController {
   
    //This is too search through the Realm Database & see what dates there is.
    func queryDatesInDatabase() {
        
        print("Query Dates in Databse Called")
        
        //This is to refer to the Database
        let dateDatabase = realm.objects(RealmEventItem.self)
        let dateFormatter = DateFormatter() // This is the coder we use to format dates
        dateFormatter.dateFormat = "dd MMMM yyyy" //This is the form in which we want the dates to be formatted to e.g. 06 August
        
        let currentDate = Date() //Refering to Today's Date so we can cross check with the dates in the Database
        let calendar = Calendar.current //stating what .day & .month are
        print("TimeZone:::; \(calendar.timeZone)")
        let currentDateComponents = calendar.dateComponents([.day,.month], from: currentDate) //Result of what day it is in the right format
//        print("Current date:::\(currentDateComponents)")
        

        for eachDate in dateDatabase { //Searching through each Event (allDates) in the Realm Database List (dateDatabease)
        
            print("For loop has been entered:")
            print("\(eachDate.nameOfEventRealm) & \(eachDate.dateOfEventRealm)")
            //This is to set what will be present in the Notification Banner
            let content = UNMutableNotificationContent()
            content.title = eachDate.nameOfEventRealm
            content.body = "Is happening: " + eachDate.dateOfEventRealm
            content.sound = UNNotificationSound.default
            
            
            
           guard let anyEventDateFormated = dateFormatter.date(from: eachDate.dateOfEventRealm)
            else {
                return print(Error.self) }
            let anyEventDateComponents = calendar.dateComponents([.day, .month, .year], from: anyEventDateFormated)
            
            
            // My Logic for Notification trigger ----------------------
            
            if eachDate.oneDayBeforeReminderRealm == true {
                //makingNotification()
                let date = FindDateBeforSomeDays(eventDate: anyEventDateFormated, DaysBefor: 1)
                print("Notification trigge Date 1 Day Before FOR FUTURE USE::::- \(date)")
                var comp2 = calendar.dateComponents([.minute, .hour, .day, .month, .year], from: date)
                
                comp2.hour = 9;
                comp2.minute = 0;
                                
                let FutureTrigger = UNCalendarNotificationTrigger(dateMatching: comp2, repeats: false)
                let FutureRequest = UNNotificationRequest(identifier: "\(eachDate.nameOfEventRealm) 1Day", content: content, trigger: FutureTrigger)
                
                UNUserNotificationCenter.current().add(FutureRequest) { (error) in
                    if(error != nil)
                    {
                        print("ERROR = " + (error?.localizedDescription)! as Any)
                    }
                    
                }
                print("They want to be reminded the day before the event")
            }
            else
            {
                print("No Remainder the Day Before the Event")
            }
            if eachDate.sevenDaysBeforeReminderRealm == true {
                
                //makingNotification()
                let date = FindDateBeforSomeDays(eventDate: anyEventDateFormated, DaysBefor: 7)
                print("Notification trigge Date 7 Day Before FOR FUTURE USE::::- \(date)")
                var comp2 = calendar.dateComponents([.minute, .hour, .day, .month, .year], from: date)
                
                comp2.hour = 9;
                comp2.minute = 0;
                
                let FutureTrigger = UNCalendarNotificationTrigger(dateMatching: comp2, repeats: false)
                
                let FutureRequest = UNNotificationRequest(identifier: "\(eachDate.nameOfEventRealm) 7Day", content: content, trigger: FutureTrigger)
                UNUserNotificationCenter.current().add(FutureRequest) { (error) in
                    if(error != nil)
                    {
                        print("ERROR = " + (error?.localizedDescription)! as Any)
                    }
                }
                print("Remind Seven Days Before")
            }
            else {
                print("Don't want to be remainded 7 Days Before \n OR \n NO ALERT" )
                
            }
            
            // ------------------------------------
            
            
            
            
            
            // Your code for Notification Trigger -----------------------------
            
//            if (IsTodaysNotiTriggered == false)
//            {
//                
//                // Your noti trigger
//                
//                let CurrentTrigger = UNTimeIntervalNotificationTrigger(timeInterval: TimeInterval(5), repeats: false) // so when we find that the dates match in the way we would like them to (i.e. 1 day before or 7 days before) then we send a notification in a second
//                //
//                
//                
//                // **Your request statement is here**
//                
//                let requestForCurrent = UNNotificationRequest(identifier: eachDate.nameOfEventRealm, content: content, trigger: CurrentTrigger)
//                
//                //
//                
//                if (currentDateComponents.day! + 1 == anyEventDateComponents.day) && (currentDateComponents.month ==
//                    anyEventDateComponents.month) { print("DAY Before")
//                    
//                    if eachDate.oneDayBeforeReminderRealm == true {
//                        //makingNotification()
//                        UNUserNotificationCenter.current().add(requestForCurrent) { (error) in
//                            if(error != nil)
//                            {
//                                print(error?.localizedDescription as Any)
//                            }
//                        }
//                        
//                        print("They want to be reminded the day before the event")
//                    } else {print("No Remainder the Day Before the Event")}
//                    
//                } else if (currentDateComponents.day! + 7 == anyEventDateComponents.day) && (currentDateComponents.month == anyEventDateComponents.month) {
//                    print("SEVEN DAYS Before")
//                    
//                    if eachDate.sevenDaysBeforeReminderRealm == true {
//                        //makingNotification()
//                        UNUserNotificationCenter.current().add(requestForCurrent) { (error) in
//                            if(error != nil)
//                            {
//                                print(error?.localizedDescription as Any)
//                            }
//                        }
//                        
//                        print("Remind Seven Days Before")
//                    } else { print("Don't want to be remainded 7 Days Before" )}
//                    
//                } else {
//                    print("NO ALERT")
//                }
//            }
            
           // ------------------------------------------
            
        }
//        IsTodaysNotiTriggered = true;
        UNUserNotificationCenter.current().getPendingNotificationRequests { (requests) in
            for request in requests {
                print("Pending Requests::: \n \(request)")
            }
        }
    }
    
    // It will count date for notification should fire
    func FindDateBeforSomeDays(eventDate:Date, DaysBefor:Int) -> Date {
        let calculatedDate : Date  = Calendar.current.date(byAdding: .day, value: -(DaysBefor), to: eventDate)!
        return calculatedDate
    }
}
        
       

   
    

