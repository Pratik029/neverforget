//
//  GetTimeintervalFromDate.swift
//  NeverForget
//
//  Created by pratik gadhesariya on 19/02/19.
//  Copyright © 2019 Hannah Stanton. All rights reserved.
//

import Foundation

extension String {
    
    func epoch(dateFormat: String = "d MMMM yyyy", timeZone: String? = nil) -> TimeInterval? {
        // building the formatter
        let formatter = DateFormatter()
        formatter.dateFormat = dateFormat
//        if let timeZone = timeZone { formatter.timeZone = TimeZone.current }
        formatter.timeZone = TimeZone.current
        // extracting the epoch
        let date = formatter.date(from: self)
        return date?.timeIntervalSinceReferenceDate
//        return date?.timeIntervalSince1970
    }
    
}

