

import RealmSearchViewController
import RealmSwift
import UIKit

//This is an extension to control the Search Bar at the top of the Table View.
extension MainTableViewController: UISearchBarDelegate {

    //This is where we edit what the search bar looks like:
    func searchBarSetUp () {
        searchTableViewBar.placeholder = "Search"
        searchTableViewBar.searchBarStyle = .prominent
        searchTableViewBar.barStyle = .black
        searchTableViewBar.keyboardAppearance = .dark
        searchTableViewBar.delegate = self
        searchTableViewBar.returnKeyType = .done
    }
    
    //This is where we state what typing in the search bar should do i.e. make the table view only show the items which have the same letters as being typed into the search bar.
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchText != "" { searching = true } else { searching = false}
        tableView.reloadData()
        if searching {
            let predicate = NSPredicate(format: "nameOfEventRealm BEGINSWITH [c]%@", searchTableViewBar.text!)
            do {
                filteredRealmResults = try Realm().objects(RealmEventItem.self).filter(predicate)
                tableView.reloadData()
                print("Search is Being Called")
            } catch {
                print("Error Filtering")
            }
        }
        

    }
    
    //This controls what happens when the cancel button next to the search bar is pressed: Basically we delete any text which is in the text bar and then hide the keyboard.
   
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searching = false
        searchBar.text = ""
        tableView.reloadData()
        searchBar.resignFirstResponder()
        
    }
    
    //This controls what happens when the done button on the keyboard is pressed: Basically we just hide the keyboard.
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        searchTableViewBar.resignFirstResponder()
    }
    
    
    
    
}
