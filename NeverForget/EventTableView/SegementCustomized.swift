//
//  SegementCustomized.swift
//  NeverForget
//
//  Created by Hannah Stanton on 16/02/2019.
//  Copyright © 2019 Hannah Stanton. All rights reserved.
//

import BetterSegmentedControl

extension MainTableViewController {
    
   // x: Int, y: Int, width: 80, height: 35
    
    func makingASegemntedControl(x: Int, y: Int, width: Int, height: Int) {
        
        let control = BetterSegmentedControl(
            frame: CGRect(x: x, y: y, width: width, height: height),
            segments: LabelSegment.segments(withTitles: ["Yes", "No"],
                                            normalFont: UIFont(name: "Avenir-Medium", size: 14.0)!,
                                            normalTextColor: .darkGray,
                                            selectedFont: UIFont(name: "Avenir-Heavy", size: 14.0)!,
                                            selectedTextColor: .white),
            index: 1,
            options: [.backgroundColor(.lightGray),
                      .indicatorViewBackgroundColor(.black)])
        
        tableView.addSubview(control)
        
    }
    
    
    
}
