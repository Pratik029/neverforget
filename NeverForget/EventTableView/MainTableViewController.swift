

import UIKit
import UserNotifications
import RealmSwift
import RealmSearchViewController
import BetterSegmentedControl

//Global Variables:
let realm = try! Realm()
let realmEventItem = RealmEventItem()
//RealmEventList is our = Datasource
//var realmEventList: Results <RealmEventItem> { get { return realm.objects(RealmEventItem.self)} }
var realmEventList = realm.objects(RealmEventItem.self)
var filteredRealmResults = realm.objects(RealmEventItem.self)
//var IsTodaysNotiTriggered:Bool = false;

class MainTableViewController: UITableViewController {
    
    var ShouldFireSegmentAction = true; //we need to fire action of segment only when user menually change the value of segment, so I taken this boolean
    
    var realmEventItem = RealmEventItem()
    let eventItemCellLayout = EventItemCellLayout()
    let monthPicker = ["Select", "January","February","March","April","May","June","July","August","September","October","November","December"]
    let dayPicker = ["Select","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20", "21","22","23","24","25","26","27","28","29","30","31"]
    var oneDayPicker = ["Select", "Yes", "No"]
    var sevenDayPicker = ["Select", "Yes", "No"]
    
    var nameOfEvent = UITextField()
    var dayOfEvent = UITextField()
    var monthOfEvent = UITextField()
    var oneDayReminder = UITextField()
    var sevenDayReminder = UITextField()
    
    var dayPickerView = UIPickerView()
    var monthPickerView = UIPickerView()
    var oneDayPickerView = UIPickerView()
    var sevenDayPickerView = UIPickerView()

    //Label which will tell the user that the table view is empty
    var labelWhenTableViewIsEmpty = UILabel()
    
    // We want to be able to search the table view so we add a search bar
    @IBOutlet weak var searchTableViewBar: UISearchBar!
    var searching = false
    var filteredRealmResults = realm.objects(RealmEventItem.self)

    override func viewDidLoad() {
        super.viewDidLoad()
        print("Realm Path : \(String(describing: realm.configuration.fileURL?.absoluteURL))")
        UNUserNotificationCenter.current().delegate = self
        UNUserNotificationCenter.current().removeAllPendingNotificationRequests();
        
       // setting tags to each picker & making the delegate self
        dayPickerView.delegate = self
        dayPickerView.tag = 1
        
        monthPickerView.delegate = self
        monthPickerView.tag = 2
        
        oneDayPickerView.delegate = self
        oneDayPickerView.tag = 3
        
        sevenDayPickerView.delegate = self
        sevenDayPickerView.tag = 4
       
        queryDatesInDatabase()
//        UNUserNotificationCenter.current().getPendingNotificationRequests { (requests) in
//            for request in requests {
//                print(request)
//            }
//        }
        //getPendingNotificationRequests
        emptyTableViewLabel()
        searchBarSetUp()
        shallWeScroll()

    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        let eventCell = tableView.dequeueReusableCell(withIdentifier: "EventCellLayout", for: indexPath) as! EventItemCellLayout
        let oneDayControl = BetterSegmentedControl(frame: CGRect(x: 310, y: 25, width: 80, height: 35),
                                                   segments: LabelSegment.segments(withTitles: ["Yes", "No"],
                                                                                   normalFont: UIFont(name: "Avenir-Medium", size: 14.0)!, normalTextColor: .darkGray,
                                                                                   selectedFont: UIFont(name: "Avenir-Heavy", size: 14.0)!, selectedTextColor: .white),
                                                   index: 1, options: [.backgroundColor(.lightGray), .indicatorViewBackgroundColor(.black)])
        //NEED TO ADD CONSTRAINTS *********
        let sevenDayControl = BetterSegmentedControl(frame: CGRect(x: 310, y: 70, width: 80, height: 35),
                                                     segments: LabelSegment.segments(withTitles: ["Yes", "No"],
                                                                                     normalFont: UIFont(name: "Avenir-Medium", size: 14.0)!, normalTextColor: .darkGray,
                                                                                     selectedFont: UIFont(name: "Avenir-Heavy", size: 14.0)!, selectedTextColor: .white),
                                                     index: 1, options: [.backgroundColor(.lightGray), .indicatorViewBackgroundColor(.black)])
        
        // We will set tag for these segment controles
        oneDayControl.tag = indexPath.row;
        sevenDayControl.tag = indexPath.row;
                
        // We have to add actions to these segment controls
        oneDayControl.addTarget(self, action: #selector(self.oneDayControlSegmentAction(sender:)), for: .valueChanged)
        sevenDayControl.addTarget(self, action: #selector(self.sevenDayControlSegmentAction(sender:)), for: .valueChanged)
        
        eventCell.addSubview(oneDayControl)
        eventCell.addSubview(sevenDayControl)
        shallWeScroll()
     
        // NEW CODE allows us to reload the Table View depending on whether we are searching or not:
        if searching {
                realmEventItem = filteredRealmResults[indexPath.row]
        } else {
                realmEventItem = realmEventList[indexPath.row]
        }
        
        //Setting parts of Realm = Layout
        eventCell.nameOfEventLabel.text! = realmEventItem.nameOfEventRealm
        eventCell.dateOfEventLabel.text! = realmEventItem.dateOfEventRealm
        //Toggle Stuff Eventually needs to be removed
        if realmEventItem.oneDayBeforeReminderRealm == true {
            ShouldFireSegmentAction = false;
            oneDayControl.setIndex(0)
        }
        else {
            ShouldFireSegmentAction = false;
            oneDayControl.setIndex(1)
        }
        if realmEventItem.sevenDaysBeforeReminderRealm == true {
            ShouldFireSegmentAction = false;
            sevenDayControl.setIndex(0)
        }
        else {
            ShouldFireSegmentAction = false;
            sevenDayControl.setIndex(1)
        }
       ShouldFireSegmentAction = true;
        return eventCell
       
    }
    
    
    
    override func numberOfSections(in tableView: UITableView) -> Int { return 1 }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searching {
            return filteredRealmResults.count
        } else {
            return realmEventList.count
        }
    }
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool { return true }
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        if (editingStyle == .delete) {
            let eventListItem = realmEventList[indexPath.row]
            try! realm.write {
                realm.delete(eventListItem)
                shallWeScroll()
            }
            tableView.deleteRows(at: [indexPath], with: .automatic)
            emptyTableViewLabel()
  

        }
    }
    
    // MARK: Segments Actions
    @objc func oneDayControlSegmentAction(sender: BetterSegmentedControl) {
        if ShouldFireSegmentAction {
            // if user changed value of segment than and than this action will fire
            let eventListItem = realmEventList[sender.tag]
            try! realm.write {
                if (sender.index == 0)
                {
                    eventListItem.oneDayBeforeReminderRealm = true;
                }
                else if (sender.index == 1)
                {
                    eventListItem.oneDayBeforeReminderRealm = false;
                }
                shallWeScroll()
            }
        }
    }
    
    @objc func sevenDayControlSegmentAction(sender: BetterSegmentedControl) {
        if ShouldFireSegmentAction {
            // if user changed value of segment than and than this action will fire
            let eventListItem = realmEventList[sender.tag]
            try! realm.write {
                if (sender.index == 0)
                {
                    eventListItem.sevenDaysBeforeReminderRealm = true;
                }
                else if (sender.index == 1)
                {
                    eventListItem.sevenDaysBeforeReminderRealm = false;
                }
                shallWeScroll()
            }
        }
    }
    
    @IBAction func addItemPressed(_ sender: UIBarButtonItem) {
        createNewEventAlertView()
    }
    
 
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return self.searchTableViewBar
    }
    
  
    
    func emptyTableViewLabel() {
        // This code is so that when there is nothing in the Table View we can present a label saying that to the user
        labelWhenTableViewIsEmpty = UILabel(frame: CGRect(x: self.view.bounds.width/2, y: self.view.bounds.height/2, width: self.view.bounds.height/6, height: self.view.bounds.width))
        if realmEventList.count == 0 {
            print("There is no Items in the list")
            self.view.addSubview(labelWhenTableViewIsEmpty)
            labelWhenTableViewIsEmpty.text = "You have no events!"
            labelWhenTableViewIsEmpty.font = UIFont(name: "Avenir-Medium", size: 25)
            labelWhenTableViewIsEmpty.textAlignment = NSTextAlignment.center
            self.tableView.backgroundView = labelWhenTableViewIsEmpty
        } else {
            labelWhenTableViewIsEmpty.removeFromSuperview()
        }
    }
    
    func shallWeScroll() {
        searchTableViewBar.isHidden = false
        tableView.alwaysBounceVertical = false
        if (tableView.contentSize.height < tableView.frame.size.height) {
            tableView.isScrollEnabled = false
        }
        else {
            tableView.isScrollEnabled = true
        }
    }
   
    func userNotificationCenter(_ center: UNUserNotificationCenter, openSettingsFor notification: UNNotification?) {
        print("openSettingsFor")
    }
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print("didReceive")
    }
}

