
import UIKit
import PMAlertController
import RealmSwift

extension MainTableViewController {
    
    func createNewEventAlertView () {
    
        let newEventAlert = PMAlertController(title: "New Event Alert", description: "", image: nil, style: .alert)
        newEventAlert.addTextField { (eventNameAlertTextField) in
            eventNameAlertTextField?.placeholder = "Event Name"
            eventNameAlertTextField?.adjustsFontSizeToFitWidth = true
            eventNameAlertTextField?.textAlignment = .center
            eventNameAlertTextField?.keyboardAppearance = .dark
            eventNameAlertTextField?.text! = nameOfEvent.text!
            nameOfEvent = eventNameAlertTextField!
        }
        newEventAlert.addTextField { (dayEventDateTextField) in
            dayEventDateTextField?.placeholder = "Day Of The Month"
            dayEventDateTextField?.inputView = dayPickerView
            dayOfEvent = dayEventDateTextField!
        }
        newEventAlert.addTextField { (monthEventDateTextField) in
            monthEventDateTextField?.placeholder = "Month"
            monthEventDateTextField?.inputView = monthPickerView
            monthOfEvent = monthEventDateTextField!
        }
        newEventAlert.addTextField { (remindOneDayBeforeTextField) in
            remindOneDayBeforeTextField?.placeholder = "Remind 1 Day Before?"
            remindOneDayBeforeTextField?.inputView = oneDayPickerView
            oneDayReminder = remindOneDayBeforeTextField!
        }
        newEventAlert.addTextField { (remindSevenDaysBeforeTextField) in
            remindSevenDaysBeforeTextField?.placeholder = "Remind 7 Days Before?"
            remindSevenDaysBeforeTextField?.inputView = sevenDayPickerView
            sevenDayReminder = remindSevenDaysBeforeTextField!
        }
        
        
        newEventAlert.addAction(PMAlertAction(title: "Add", style: .default, action: {
            print("Add Event Button Pressed in Alert")
            
            //Adding realmEventItem & its variables to the Database
            try! realm.write {
                
      
               //The concept of adding a newEventItem of type RealmEventItem() & and then adding all the values to it rather than direct to the RealmEventItem() fixed the problem of the new items replacing the previous ones.
                let newEventItem = RealmEventItem()
                
                // Joining the Day & Month and current year Together
                let year = Date().currentYear
                let fullDateOfEvent = self.dayOfEvent.text! + " " + self.monthOfEvent.text! + " " + year!
                // Making Variables to give a boolean of whether the user would like reminders
                var oneDayReminderItem = false
                var sevenDayReminderItem = false
                
                if self.oneDayReminder.text == "Yes" {
                    oneDayReminderItem = true
                } else {oneDayReminderItem = false}
                
                if self.sevenDayReminder.text == "Yes" {
                    sevenDayReminderItem = true
                } else {sevenDayReminderItem = false}
                
                //Setting the values to the newEventItem --> which is part of the RealmEventItem()
                newEventItem.nameOfEventRealm = self.nameOfEvent.text!
                newEventItem.dateOfEventRealm = fullDateOfEvent
                newEventItem.oneDayBeforeReminderRealm = oneDayReminderItem
                newEventItem.sevenDaysBeforeReminderRealm = sevenDayReminderItem
                
                            
                // Making Sure there is something written in the Name & Date Text Fields before allowing anything to be added
            if self.nameOfEvent.text != "" && self.dayOfEvent.text != "" && self.monthOfEvent.text != "" {
                        /*
                         - use create method instead of add method to add new entries,
                         - Because add method replaces last new entry with our previous entry,
                         - so every time only one object store in database
                         */
               
                    realm.create(RealmEventItem.self, value: newEventItem, update: false)
                    self.tableView.insertRows(at: [IndexPath.init(row: realmEventList.count-1, section: 0)], with: .automatic)
                    // check for new notification trigger on add of every new event
                    self.queryDatesInDatabase()
                    //By Adding the follwing line it has meant that my code no longer crashes when I add an Item, then delete it & then try and add another item again.
                    self.tableView.numberOfRows(inSection: realmEventList.count)
                    print(realmEventList.count)
                
                    self.labelWhenTableViewIsEmpty.text = ""
                        print("Add to Table View Called")
                    self.shallWeScroll()
                    
                } else {
                    // Missing Boxes
                    self.generalUserAlert(messege: "Missing Information")
                    print("Missing Information!")
                }
            
                // This makes sure that each time the user goes to add a new Event all the picker are reset
                if self.nameOfEvent.text != "" { self.nameOfEvent.text = "" }
                if self.dayPickerView.selectedRow(inComponent: 0) != 0 { self.dayPickerView.selectRow(0, inComponent: 0, animated: false) }
                if self.monthPickerView.selectedRow(inComponent: 0) != 0 {self.monthPickerView.selectRow(0, inComponent: 0, animated: false)}
                if self.oneDayPickerView.selectedRow(inComponent: 0) != 0 {self.oneDayPickerView.selectRow(0, inComponent: 0, animated: false)}
                if self.sevenDayPickerView.selectedRow(inComponent: 0) != 0 {self.sevenDayPickerView.selectRow(0, inComponent: 0, animated: false)}
          
                }

            print("REAL DATA:::-- \(realm.objects(RealmEventItem.self))")
            
        }))
        
        
        newEventAlert.addAction(PMAlertAction(title: "Cancel", style: .cancel, action: {
            print("Cancel Button Pressed in Alert")
        }))
        
        present(newEventAlert, animated: true, completion: nil)
    
    }
}
extension Date {
    
    // Year
    var currentYear: String? {
//        return getDateComponent(dateFormat: "yyyy")
        return getDateComponent(dateFormat: "yyyy")
    }
    
    
    func getDateComponent(dateFormat: String) -> String? {
        let format = DateFormatter()
        format.dateFormat = dateFormat
        return format.string(from: self)
    }
    
    
}
