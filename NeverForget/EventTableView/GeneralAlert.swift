

import UIKit
import PMAlertController

extension MainTableViewController {
    
    //General Alert Template where the string is passed elsewhere
    func generalUserAlert (messege: String) {
        
        let alert = PMAlertController(title: "Alert", description: messege, image: nil, style: .alert)
        alert.gravityDismissAnimation = true
        alert.addAction(PMAlertAction(title: "Ok", style: .cancel, action: {
            print("Ok Pressed")

        }))
        
        present(alert, animated: true, completion: nil)
            
    }

    
    
    
    
}
