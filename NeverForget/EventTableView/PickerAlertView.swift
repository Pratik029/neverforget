
import UIKit

extension MainTableViewController: UIPickerViewDataSource, UIPickerViewDelegate {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == 1 { return dayPicker.count }
        else if pickerView.tag == 2 { return monthPicker.count }
        else if pickerView.tag == 3 { return oneDayPicker.count }
        else if pickerView.tag == 4 { return sevenDayPicker.count }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView.tag == 1 { return String(dayPicker[row]) }
        else if pickerView.tag == 2 { return monthPicker[row] }
        else if pickerView.tag == 3 { return oneDayPicker[row] }
        else if pickerView.tag == 4 { return sevenDayPicker[row] }
        return nil
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView.tag == 1 {
            dayOfEvent.text = String(dayPicker[row])
        } else if pickerView.tag == 2 {
            monthOfEvent.text = monthPicker[row]
        } else if pickerView.tag == 3 {
            oneDayReminder.text = oneDayPicker[row]
            if oneDayReminder.text == "Select" {oneDayReminder.text = nil}
        } else if pickerView.tag == 4 {
            sevenDayReminder.text = sevenDayPicker[row]
            if oneDayReminder.text == "Select" {oneDayReminder.text = nil}
        }
    }
    
}
