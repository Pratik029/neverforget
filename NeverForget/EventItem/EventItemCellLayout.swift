

import UIKit
import BetterSegmentedControl

class EventItemCellLayout: UITableViewCell {
    
    @IBOutlet weak var nameOfEventLabel: UILabel!
    @IBOutlet weak var dateOfEventLabel: UILabel!
    
}
